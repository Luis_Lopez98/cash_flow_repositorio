/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.util.ArrayList;

/**
 *
 * @author andre
 */
public class FlujoMensual {
    
    ArrayList<Flujo> semana1;
    ArrayList<Flujo> semana2;
    ArrayList<Flujo> semana3;
    ArrayList<Flujo> semana4;
    double totalSemana1;
    double totalSemana2;
    double totalSemana3;
    double totalSemana4;

    public FlujoMensual() {
        this.semana1 = new ArrayList<>();
        this.semana2 = new ArrayList<>();
        this.semana3 = new ArrayList<>();
        this.semana4 = new ArrayList<>();
        this.totalSemana1=0;
        this.totalSemana2=0;
        this.totalSemana3=0;
        this.totalSemana4=0;
    }
    
    public void addSemana1(Flujo flujo){
        this.semana1.add(flujo);
        totalSemana1+=flujo.getMonto();
    }
    public void addSemana2(Flujo flujo){
        this.semana2.add(flujo);
        totalSemana2+=flujo.getMonto();
    }
    public void addSemana3(Flujo flujo){
        this.semana3.add(flujo);
        totalSemana3+=flujo.getMonto();
    }
    public void addSemana4(Flujo flujo){
        this.semana4.add(flujo);
        totalSemana4+=flujo.getMonto();
    }

    public ArrayList<Flujo> getSemana1() {
        return semana1;
    }

    public ArrayList<Flujo> getSemana2() {
        return semana2;
    }

    public ArrayList<Flujo> getSemana3() {
        return semana3;
    }

    public ArrayList<Flujo> getSemana4() {
        return semana4;
    }
    
    
    public double getTotalSemana1() {
        return totalSemana1;
    }

    public double getTotalSemana2() {
        return totalSemana2;
    }

    public double getTotalSemana3() {
        return totalSemana3;
    }

    public double getTotalSemana4() {
        return totalSemana4;
    }

    public double getTotal() {
        return totalSemana1+totalSemana2+totalSemana3+totalSemana4;
    }
    public void setTotalSemana4(){
        for (Flujo flujo : semana4) {
            totalSemana4+= flujo.getMonto();
        }
    }
    
}
