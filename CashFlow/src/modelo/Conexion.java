/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.ObservableList;

/**
 *
 * @author Luis Manuel
 */
public class Conexion {

    private static final String driver = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
    private static final String url = "jdbc:sqlserver://THOMAS:1433;databaseName=CashFlowDB";
    private static final String username = "admin2";
    private static final String password = "1234";
    private static Connection connection = null;

    public static Conexion conexion;
    private String table;

    public Conexion() {
    }

    public static Connection getConnection() {
        try {
            if (connection == null) {
                Class.forName(driver);
                connection = DriverManager.getConnection(url, username, password);
            }
            return connection;
        } catch (ClassNotFoundException | SQLException e) {
        }
        return connection;
    }

    public static boolean setCategoria(Categoria categoria) {
        boolean status = false;
        String query = "insert into categorias (Clasificacion,Categoria,SubCategoria) values(?,?,?) ";
        try {
            PreparedStatement ps = connection.prepareStatement(query);
            ps.setString(1, categoria.getClasificacion());
            ps.setString(2, categoria.getCategoria());
            ps.setString(3, categoria.getSubcategoria());
            ps.executeUpdate();
            status = true;
        } catch (SQLException ex) {
            Logger.getLogger(Conexion.class.getName()).log(Level.SEVERE, null, ex);
        }

        return status;
    }

    public static ArrayList<Categoria> getCategorias() {
        boolean status = false;
        ArrayList<Categoria> listaCategorias = new ArrayList<>();
        String query = "select * from categorias";

        try {
            PreparedStatement ps = connection.prepareStatement(query);
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                Categoria c = new Categoria(
                        ((int) rs.getObject("id")),
                        ((String) rs.getObject("Clasificacion")),
                        ((String) rs.getObject("Categoria")),
                        ((String) rs.getObject("subcategoria"))
                );
                listaCategorias.add(c);
            }

            if (!listaCategorias.isEmpty()) {
                return listaCategorias;
            }

            status = true;
        } catch (SQLException ex) {
            Logger.getLogger(Conexion.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    public static boolean setFlujo(Flujo flujo) {
        boolean status = false;

        String query = "insert into flujoefectivo (fecha,descripcion,categoria,subcategoria,monto,clasificacion) values(?,?,?,?,?,?) ";

        try {
            PreparedStatement ps = connection.prepareStatement(query);
            ps.setString(1, flujo.getFecha());
            ps.setString(2, flujo.getDescripcion());
            ps.setString(3, flujo.getCategoria());
            ps.setString(4, flujo.getSubcategoria());
            ps.setString(5, flujo.getMonto() + "");
            ps.setString(6, flujo.getClasificacion());
            ps.executeUpdate();
            status = true;
        } catch (SQLException ex) {
            Logger.getLogger(Conexion.class.getName()).log(Level.SEVERE, null, ex);
        }

        return status;
    }

    public static ArrayList<Flujo> getFlujos() {
        boolean status = false;
        ArrayList<Flujo> listaFlujo = new ArrayList<>();
        String query = "select * from flujoefectivo";

        try {
            PreparedStatement ps = connection.prepareStatement(query);
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                Flujo f = new Flujo();
                f.setId(((int) rs.getObject("id")));
                f.setCategoria(((String) rs.getObject("categoria")));
                f.setClasificacion(((String) rs.getObject("clasificacion")));
                f.setDescripcion(((String) rs.getObject("descripcion")));
                f.setMonto(((Double) rs.getObject("monto")));
                f.setFecha(((String) rs.getObject("fecha")));
                f.setSubcategoria(((String) rs.getObject("subcategoria")));

                        
                        
                        
                listaFlujo.add(f);
            }

            if (!listaFlujo.isEmpty()) {
                return listaFlujo;
            }

            status = true;
        } catch (SQLException ex) {
            Logger.getLogger(Conexion.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    public static FlujoMensual getFlujos(int option, String mes, String ano) {
        boolean status = false;
        FlujoMensual fl = new FlujoMensual();
        String op = "salida";
        String iniciaEn = "";
        if (option == 1) {
            op = "ingreso";
        }else{
            iniciaEn="viernes";
        }
        System.out.println("obteniendo: "+op);
        String fecha = "32/" + mes + "/" + ano;
        String query = "select * from flujoefectivo where clasificacion = ? and fecha < ? ";

        try {
            PreparedStatement ps = connection.prepareStatement(query);            
            ps.setString(1, op);
            ps.setString(2, fecha);

            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                Flujo f = new Flujo(
                        ((int) rs.getObject("id")),
                        ((Double) rs.getObject("monto")),
                        ((String) rs.getObject("fecha")),
                        ((String) rs.getObject("descripcion")),
                        ((String) rs.getObject("categoria")),
                        ((String) rs.getObject("subcategoria")),
                        ((String) rs.getObject("clasificacion")),
                        ((String) rs.getObject("dia")));
                
                System.out.println(f);
                        
                if (Integer.parseInt(f.getFecha().substring(0, 2)) == 1 && f.getDia().toLowerCase().equals("domingo")) {
                    iniciaEn = "domingo";
                    System.out.println(" entra linea 187");
                }
                if (Integer.parseInt(f.getFecha().substring(0, 2)) == 1 && f.getDia().toLowerCase().equals("lunes")) {
                    iniciaEn = "lunes";
                    System.out.println(" entra linea 191");
                }
                if (Integer.parseInt(f.getFecha().substring(0, 2)) == 1 && f.getDia().toLowerCase().equals("martes")) {
                    iniciaEn = "martes";
                    System.out.println(" entra linea 195");
                }
                if (Integer.parseInt(f.getFecha().substring(0, 2)) == 1 && f.getDia().toLowerCase().equals("miercoles")) {
                    iniciaEn = "miercoles";
                    System.out.println(" entra linea 199");
                }
                if (Integer.parseInt(f.getFecha().substring(0, 2)) == 1 && f.getDia().toLowerCase().equals("jueves")) {
                    iniciaEn = "jueves";
                    System.out.println(" entra linea 200");
                }
                if (Integer.parseInt(f.getFecha().substring(0, 2)) == 1 && f.getDia().toLowerCase().equals("viernes")) {
                    iniciaEn = "viernes";
                    System.out.println(" entra linea 206");
                }
                if (Integer.parseInt(f.getFecha().substring(0, 2)) == 1 && f.getDia().toLowerCase().equals("sabado")) {
                    iniciaEn = "sabado";
                    System.out.println(" entra linea 211");
                }

                if (iniciaEn.toLowerCase().equals("domingo")) {
                    if (Integer.parseInt(f.getFecha().substring(0, 2)) > 0 && Integer.parseInt(f.getFecha().substring(0, 2)) < 8) {
                        fl.addSemana1(f);
                    }
                    if (Integer.parseInt(f.getFecha().substring(0, 2)) > 7 && Integer.parseInt(f.getFecha().substring(0, 2)) < 15) {
                        fl.addSemana2(f);
                    }
                    if (Integer.parseInt(f.getFecha().substring(0, 2)) > 14 && Integer.parseInt(f.getFecha().substring(0, 2)) < 22) {
                        fl.addSemana3(f);
                    }
                    if (Integer.parseInt(f.getFecha().substring(0, 2)) > 21 && Integer.parseInt(f.getFecha().substring(0, 2)) < 32) {
                        fl.addSemana4(f);
                    }
                }
                if (iniciaEn.toLowerCase().equals("lunes")) {
                    if (Integer.parseInt(f.getFecha().substring(0, 2)) > 0 && Integer.parseInt(f.getFecha().substring(0, 2)) < 7) {
                        fl.addSemana1(f);
                    }
                    if (Integer.parseInt(f.getFecha().substring(0, 2)) > 6 && Integer.parseInt(f.getFecha().substring(0, 2)) < 14) {
                        fl.addSemana2(f);
                    }
                    if (Integer.parseInt(f.getFecha().substring(0, 2)) > 13 && Integer.parseInt(f.getFecha().substring(0, 2)) < 21) {
                        fl.addSemana3(f);
                    }
                    if (Integer.parseInt(f.getFecha().substring(0, 2)) > 20 && Integer.parseInt(f.getFecha().substring(0, 2)) < 32) {
                        fl.addSemana4(f);
                    }
                }
                if (iniciaEn.toLowerCase().equals("martes")) {
                    if (Integer.parseInt(f.getFecha().substring(0, 2)) > 0 && Integer.parseInt(f.getFecha().substring(0, 2)) < 6) {
                        fl.addSemana1(f);
                    }
                    if (Integer.parseInt(f.getFecha().substring(0, 2)) > 5 && Integer.parseInt(f.getFecha().substring(0, 2)) < 13) {
                        fl.addSemana2(f);
                    }
                    if (Integer.parseInt(f.getFecha().substring(0, 2)) > 12 && Integer.parseInt(f.getFecha().substring(0, 2)) < 20) {
                        fl.addSemana3(f);
                    }
                    if (Integer.parseInt(f.getFecha().substring(0, 2)) > 19 && Integer.parseInt(f.getFecha().substring(0, 2)) < 32) {
                        fl.addSemana4(f);
                    }
                }
                if (iniciaEn.toLowerCase().equals("miercoles")) {
                    if (Integer.parseInt(f.getFecha().substring(0, 2)) > 0 && Integer.parseInt(f.getFecha().substring(0, 2)) < 5) {
                        fl.addSemana1(f);
                    }
                    if (Integer.parseInt(f.getFecha().substring(0, 2)) > 4 && Integer.parseInt(f.getFecha().substring(0, 2)) < 12) {
                        fl.addSemana2(f);
                    }
                    if (Integer.parseInt(f.getFecha().substring(0, 2)) > 11 && Integer.parseInt(f.getFecha().substring(0, 2)) < 19) {
                        fl.addSemana3(f);
                    }
                    if (Integer.parseInt(f.getFecha().substring(0, 2)) > 18 && Integer.parseInt(f.getFecha().substring(0, 2)) < 32) {
                        fl.addSemana4(f);
                    }
                }
                if (iniciaEn.toLowerCase().equals("jueves")) {
                    if (Integer.parseInt(f.getFecha().substring(0, 2)) > 0 && Integer.parseInt(f.getFecha().substring(0, 2)) < 4) {
                        fl.addSemana1(f);
                    }
                    if (Integer.parseInt(f.getFecha().substring(0, 2)) > 3 && Integer.parseInt(f.getFecha().substring(0, 2)) < 11) {
                        fl.addSemana2(f);
                    }
                    if (Integer.parseInt(f.getFecha().substring(0, 2)) > 10 && Integer.parseInt(f.getFecha().substring(0, 2)) < 18) {
                        fl.addSemana3(f);
                    }
                    if (Integer.parseInt(f.getFecha().substring(0, 2)) > 17 && Integer.parseInt(f.getFecha().substring(0, 2)) < 32) {
                        fl.addSemana4(f);
                    }
                }
                if (iniciaEn.toLowerCase().equals("viernes")) {
                    if (Integer.parseInt(f.getFecha().substring(0, 2)) > 0 && Integer.parseInt(f.getFecha().substring(0, 2)) < 3) {
                        fl.addSemana1(f);
                    }
                    if (Integer.parseInt(f.getFecha().substring(0, 2)) > 2 && Integer.parseInt(f.getFecha().substring(0, 2)) < 10) {
                        fl.addSemana2(f);
                    }
                    if (Integer.parseInt(f.getFecha().substring(0, 2)) > 9 && Integer.parseInt(f.getFecha().substring(0, 2)) < 17) {
                        fl.addSemana3(f);
                    }
                    if (Integer.parseInt(f.getFecha().substring(0, 2)) > 16 && Integer.parseInt(f.getFecha().substring(0, 2)) < 32) {
                        fl.addSemana4(f);
                    }
                }
                if (iniciaEn.toLowerCase().equals("sabado")) {
                    if (Integer.parseInt(f.getFecha().substring(0, 2)) > 0 && Integer.parseInt(f.getFecha().substring(0, 2)) < 2) {
                        fl.addSemana1(f);
                    }
                    if (Integer.parseInt(f.getFecha().substring(0, 2)) > 1 && Integer.parseInt(f.getFecha().substring(0, 2)) < 9) {
                        fl.addSemana2(f);
                    }
                    if (Integer.parseInt(f.getFecha().substring(0, 2)) > 8 && Integer.parseInt(f.getFecha().substring(0, 2)) < 16) {
                        fl.addSemana3(f);
                    }
                    if (Integer.parseInt(f.getFecha().substring(0, 2)) > 15 && Integer.parseInt(f.getFecha().substring(0, 2)) < 32) {
                        fl.addSemana4(f);
                    }
                }
            }

            status = true;
        } catch (SQLException ex) {
            Logger.getLogger(Conexion.class.getName()).log(Level.SEVERE, null, ex);
        }

        return fl;
    }

}
