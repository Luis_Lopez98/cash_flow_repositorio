/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author andre
 */
public class Categoria {

    private int id;
    private String clasificacion;
    private String categoria;

    private String sCategoria;

    private String subcategoria;


    public String getClasificacion() {
        return clasificacion;
    }

    public String getCategoria() {
        return categoria;
    }

    public int getId() {
        return id;
    }

    public String getSubcategoria() {
        return subcategoria;
    }

    @Override
    public String toString() {
        return  clasificacion+" - "+categoria + " - " + subcategoria ;
    }



    public Categoria(int id, String... parameters) {
        this.id = id;
        clasificacion = parameters[0];
        categoria = parameters[1];
        subcategoria = parameters[2];
    }
    
    public Categoria(String... parameters) {
        clasificacion = parameters[0];
        categoria = parameters[1];
        subcategoria = parameters[2];
    }

}
