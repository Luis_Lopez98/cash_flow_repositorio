/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author andre
 */
public class Flujo {

    private int id;
    private String fecha;
    private String descripcion;
    private String categoria;
    private String subcategoria;
    private String clasificacion;
    private String dia;
    private double monto;
    
    public Flujo(){}
    public Flujo(int id, double monto, String fecha, String descripcion, String categoria, String subcategoria, String clasificacion, String dia) {
        this.id = id;
        this.monto = monto;
        this.fecha = fecha;
        this.descripcion = descripcion;
        this.categoria = categoria;
        this.subcategoria = subcategoria;
        this.clasificacion = clasificacion;
        this.dia = dia;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public void setSubcategoria(String subcategoria) {
        this.subcategoria = subcategoria;
    }

    public void setClasificacion(String clasificacion) {
        this.clasificacion = clasificacion;
    }

    public void setDia(String dia) {
        this.dia = dia;
    }

    public void setMonto(double monto) {
        this.monto = monto;
    }

    public Flujo(int id, double monto, String fecha, String descripcion, String categoria, String subcategoria, String clasificacion) {
        this.id = id;
        this.monto = monto;
        this.fecha = fecha;
        this.descripcion = descripcion;
        this.categoria = categoria;
        this.subcategoria = subcategoria;
        this.clasificacion = clasificacion;
    }

    public Flujo(double monto, String fecha, String descripcion, String categoria, String subcategoria, String clasificacion) {
        this.monto = monto;
        this.fecha = fecha;
        this.descripcion = descripcion;
        this.categoria = categoria;
        this.subcategoria = subcategoria;
        this.clasificacion = clasificacion;
    }

    public String getDia() {
        return dia;
    }

    @Override
    public String toString() {
        return "Flujo{" + "id=" + id + ", monto=" + monto + '}';
    }

    public String getClasificacion() {
        return clasificacion;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFecha() {
        return fecha;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public String getCategoria() {
        return categoria;
    }

    public String getSubcategoria() {
        return subcategoria;
    }

    public double getMonto() {
        return monto;
    }
}
