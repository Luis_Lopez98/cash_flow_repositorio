/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javafx.collections.ObservableList;

/**
 *
 * @author andre
 */
public class Cuenta {
    int id;
    String descripcion;
    float cantidad;
     float cantidad1;
      float cantidad2;
       float cantidad3;
        float cantidad4;
        float status;

    public Cuenta() {
    }
        
        

    public Cuenta(int id, float cantidad, float cantidad1, float cantidad2, float cantidad3, float cantidad4, float status) {
        this.id = id;
        this.cantidad = cantidad;
        this.cantidad1 = cantidad1;
        this.cantidad2 = cantidad2;
        this.cantidad3 = cantidad3;
        this.cantidad4 = cantidad4;
        this.status = status;
    }
      public Cuenta(String descripcion, float cantidad, float cantidad1, float cantidad2, float cantidad3, float cantidad4, float satatus) {
        this.descripcion = descripcion;
        this.cantidad = cantidad;
        this.cantidad1 = cantidad1;
        this.cantidad2 = cantidad2;
        this.cantidad3 = cantidad3;
        this.cantidad4 = cantidad4;
        this.status = satatus;
    }

    public float getStatus() {
        return status;
    }
      

    public String getDescripcion() {
        return descripcion;
    }
        
        

    
    public int getId() {
        return id;
    }

    public float getCantidad() {
        return cantidad;
    }

    public float getCantidad1() {
        return cantidad1;
    }

    public float getCantidad2() {
        return cantidad2;
    }

    public float getCantidad3() {
        return cantidad3;
    }

    public float getCantidad4() {
        return cantidad4;
    }


  
    
     public static void mostrarCuentaCobra(Connection conect, ObservableList<Cuenta> listaP) throws SQLException{
        try {
            Statement instruccion = conect.createStatement();
            ResultSet  resultado = instruccion.executeQuery("select  ID,fecha,descripcion,cantidad, status from CuentasCobrar");
            while (resultado.next()) {
                listaP.add(new Cuenta(
                           resultado.getInt("ID"),
                           resultado.getFloat("cantidad"),
                           resultado.getFloat("cantidad"),
                           resultado.getFloat("cantidad"),
                           resultado.getFloat("cantidad"),
                           resultado.getFloat("cantidad"),
                           resultado.getFloat("status")));
                
            }
        } catch (Exception e) {
            e.printStackTrace();
        }  
    } 
      public static void mostrarCuentaPagar(Connection conect, ObservableList<Cuenta> listaP) throws SQLException{
        try {
            Statement instruccion = conect.createStatement();
            ResultSet  resultado = instruccion.executeQuery("select  ID,fecha,descripcion,cantidad, status from cuentasPagar");
            while (resultado.next()) {
                listaP.add(new Cuenta(
                           resultado.getString("descripcion"),
                           resultado.getFloat("cantidad"),
                           resultado.getFloat("cantidad"),
                           resultado.getFloat("cantidad"),
                           resultado.getFloat("cantidad"),
                           resultado.getFloat("cantidad"),
                            resultado.getFloat("status")));
                
            }
        } catch (Exception e) {
            e.printStackTrace();
        }  
    } 
    
}
