/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controladores;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;
import modelo.Conexion;

/**
 * FXML Controller class
 *
 * @author Luis Manuel
 */
public class homeController implements Initializable {

    @FXML
    private Button adminCuentasButton;
    @FXML
    private Button adminFlujoButton;
    @FXML
    private Button adminCategoria;
    @FXML
    private Button btnReport;
    
    

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        Conexion.getConnection();
    }    
    
    @FXML
    public void handleBtnCuenta() throws IOException{
        Parent root = FXMLLoader.load(getClass().getResource("/vistas/cuentaPagar.fxml"));
        Stage window = (Stage) adminCuentasButton.getScene().getWindow();
        window.setScene(new Scene(root));
    }
    
    @FXML
    public void handleBtnFlujo() throws IOException{
        Parent root = FXMLLoader.load(getClass().getResource("/vistas/flujoEfectivoView.fxml"));
        Stage window = (Stage) adminFlujoButton.getScene().getWindow();
        window.setScene(new Scene(root));
    }
    
    @FXML
    public void handleBtnCategoria() throws IOException{
        Parent root = FXMLLoader.load(getClass().getResource("/vistas/registroCategoria.fxml"));
        Stage window = (Stage) adminCategoria.getScene().getWindow();
        window.setScene(new Scene(root));
    }
    
    @FXML
    public void handleBtnReport() throws IOException{
        Parent root = FXMLLoader.load(getClass().getResource("/vistas/reporteSemanal.fxml"));
        Stage window = (Stage) adminCategoria.getScene().getWindow();
        window.setScene(new Scene(root));
    }
    
}
