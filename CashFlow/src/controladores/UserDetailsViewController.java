/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controladores;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.Initializable;
import modelo.Conexion;

/**
 * FXML Controller class
 *
 * @author Luis Manuel
 */
public class UserDetailsViewController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        Conexion.getConnection();
    }    
    
}
