/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controladores;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import modelo.Categoria;
import modelo.Conexion;
import static modelo.Conexion.conexion;

/**
 * FXML Controller class
 *
 * @author Luis Manuel
 */
public class RegistroCategoriaController implements Initializable {

    @FXML
    private TableColumn subCategoryColumn;

    @FXML
    private TableColumn clasifyColumn;

    @FXML
    private TableColumn categoryColumn;

    @FXML
    private TableView<Categoria> categoryTable;

    @FXML
    private TextField sCategoriaTxField;

    @FXML
    private TextField categoriaTxField;

    @FXML
    private Button saveButton;

    @FXML
    private ComboBox<String> comboBoxClasificacion;

    Alert alert = new Alert(Alert.AlertType.INFORMATION);

    @FXML
    private Button btnBack;

    @FXML
    public void handleBtnBack() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/vistas/home.fxml"));
        Stage window = (Stage) btnBack.getScene().getWindow();
        window.setScene(new Scene(root));
    }

    public void fullTable() {

        if (Conexion.getCategorias()!= null) {

            for (Categoria categoria : Conexion.getCategorias()) {
                lstCategory.add(categoria);
            }
            this.categoryTable.setItems(lstCategory);
        }
    }

    ObservableList<Categoria> lstCategory;

    @FXML
    void saveRegister(ActionEvent event) {
        if (!"".equals(categoriaTxField.getText()) && !"".equals(sCategoriaTxField.getText()) && comboBoxClasificacion.getSelectionModel().getSelectedItem() != null) {
            Categoria categoria = new Categoria(
                    comboBoxClasificacion.getSelectionModel().getSelectedItem(),
                    categoriaTxField.getText(),
                    sCategoriaTxField.getText()
            );
            
            conexion.setCategoria(categoria);

            if (Conexion.setCategoria(categoria)) {
                alert.setContentText("Guardado con exito");
                alert.showAndWait();
                lstCategory.add(categoria);
                this.categoryTable.setItems(lstCategory);
            } else {
                alert.setContentText("Ha ocurrido un error n:102, contacte a sistemas");
                alert.showAndWait();
            }

        } else {
            alert.setContentText("Debe llenar todos los datos");
            alert.showAndWait();
        }

        /*
        diseñar el metodo para insertar la Query y que registre la categoria
        conexion.guardarCategoria(categoria);
        de preferencia que retorne algo para mandar un mensaje que el proceso ha sido exitoso
         */
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        Conexion.getConnection();
        alert.setTitle("Informacion de la consulta");
        comboBoxClasificacion.setItems(FXCollections.observableArrayList(
                "GAO", "Costo-Venta", "Ingreso"
        ));

        lstCategory = FXCollections.observableArrayList();

        this.clasifyColumn.setCellValueFactory(new PropertyValueFactory("clasificacion"));
        this.categoryColumn.setCellValueFactory(new PropertyValueFactory("categoria"));
        this.subCategoryColumn.setCellValueFactory(new PropertyValueFactory("subcategoria"));

        fullTable();

    }

}
