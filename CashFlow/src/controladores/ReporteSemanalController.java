/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controladores;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.text.DecimalFormat;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import modelo.Categoria;
import modelo.Conexion;
import modelo.Cuenta;
import modelo.Flujo;
import modelo.FlujoMensual;

/**
 * FXML Controller class
 *
 * @author andre
 */
public class ReporteSemanalController implements Initializable {

    static Conexion conect = new Conexion();
    Connection conexion = conect.getConnection();

    @FXML
    private RadioButton radioButtonFlujoEfectivoIngresos;

    @FXML
    private RadioButton radioButtonCuentaCobrar;

    @FXML
    private TableColumn<?, ?> columnSemana4;

    @FXML
    private TableColumn<?, ?> columnSemana3;

    @FXML
    private TableColumn<?, ?> columnSemana2;

    @FXML
    private Label totalSuma;

    @FXML
    private TableColumn<?, ?> columnSemana1;

    @FXML
    private TableColumn<?, ?> totalCuentas;

    @FXML
    private RadioButton radioButtonFlujoEfectivoGastos;

    @FXML
    private Label totalSumaSemana1;

    @FXML
    private Label totalSumaSemana2;

    @FXML
    private TableView<Cuenta> tableCuentas;

    @FXML
    private ComboBox<String> comboBoxMes;

    @FXML
    private Button btnBack;

    @FXML
    private TableColumn<?, ?> idCuentas;

    @FXML
    private ComboBox<String> comboBoxAno;

    @FXML
    private ToggleGroup tggCuentas;

    @FXML
    private TableView<Flujo> tableSemana4;

    @FXML
    private TableView<Flujo> tableSemana3;

    @FXML
    private Label totalSumaSemana3;

    @FXML
    private TableColumn<?, ?> semana5Cuentas;

    @FXML
    private Label totalSumaSemana4;

    @FXML
    private TextField totalPagarCobrar;

    @FXML
    private TableView<Flujo> tableSemana2;

    @FXML
    private RadioButton radioButtonCuentaPagar;

    @FXML
    private TableView<Flujo> tableSemana1;

    @FXML
    private RadioButton radioButtonFlujoEfectivoDiferencia;

    @FXML
    private TableColumn<?, ?> semana1Cuentas;

    @FXML
    private TableColumn<?, ?> semana2Cuentas;

    @FXML
    private TableColumn<?, ?> semana3Cuentas;

    @FXML
    private TableColumn<?, ?> semana4Cuentas;

    @FXML
    private ToggleGroup tggFlujo;
    private ObservableList<Cuenta> listaCuenta;
    private static DecimalFormat df2 = new DecimalFormat("#.##");
    ObservableList<Flujo> lstFlujoSemana1;//ingresos
    ObservableList<Flujo> lstFlujoSemana2;
    ObservableList<Flujo> lstFlujoSemana3;
    ObservableList<Flujo> lstFlujoSemana4;//ingresos
    ObservableList<Flujo> lstFlujoSemana11;//gastos salidas
    ObservableList<Flujo> lstFlujoSemana22;
    ObservableList<Flujo> lstFlujoSemana33;
    ObservableList<Flujo> lstFlujoSemana44;//gastos salidas
    ObservableList<Flujo> lstFlujoSemana111;//diferencia
    ObservableList<Flujo> lstFlujoSemana222;
    ObservableList<Flujo> lstFlujoSemana333;
    ObservableList<Flujo> lstFlujoSemana444;//diferencias
    ObservableList<Cuenta> lstCuenta;
    Alert alert = new Alert(Alert.AlertType.INFORMATION);

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {

        lstFlujoSemana1 = FXCollections.observableArrayList();
        lstFlujoSemana2 = FXCollections.observableArrayList();
        lstFlujoSemana3 = FXCollections.observableArrayList();
        lstFlujoSemana4 = FXCollections.observableArrayList();
        lstFlujoSemana11 = FXCollections.observableArrayList();
        lstFlujoSemana22 = FXCollections.observableArrayList();
        lstFlujoSemana33 = FXCollections.observableArrayList();
        lstFlujoSemana44 = FXCollections.observableArrayList();
        lstFlujoSemana111 = FXCollections.observableArrayList();
        lstFlujoSemana222 = FXCollections.observableArrayList();
        lstFlujoSemana333 = FXCollections.observableArrayList();
        lstFlujoSemana444 = FXCollections.observableArrayList();
        lstCuenta = FXCollections.observableArrayList();
        columnSemana1.setCellValueFactory(new PropertyValueFactory<>("monto"));
        columnSemana2.setCellValueFactory(new PropertyValueFactory<>("monto"));
        columnSemana3.setCellValueFactory(new PropertyValueFactory<>("monto"));
        columnSemana4.setCellValueFactory(new PropertyValueFactory<>("monto"));

        comboBoxMes.setItems(FXCollections.observableArrayList(
                "01 -> Enero", "02 -> Febrero", "03 -> Marzo", "04 -> Abril",
                "05 -> Mayo", "06 -> Junio", "07 -> Julio", "08 -> Agosto",
                "09 -> Septiembre", "10 -> Octubre", "11 -> Noviembre", "12 -> Diciembre"
        ));
        comboBoxAno.setItems(FXCollections.observableArrayList(
                "2019", "2020", "2021"
        ));
    }

    private void llamarCuentaCobrar() {
        listaCuenta = FXCollections.observableArrayList();
        tableCuentas.setItems(listaCuenta);
        idCuentas.setCellValueFactory(new PropertyValueFactory<>("id"));
        semana1Cuentas.setCellValueFactory(new PropertyValueFactory("cantidad"));
        semana2Cuentas.setCellValueFactory(new PropertyValueFactory("cantidad1"));
        semana3Cuentas.setCellValueFactory(new PropertyValueFactory("cantidad2"));
        semana4Cuentas.setCellValueFactory(new PropertyValueFactory("cantidad3"));
        semana5Cuentas.setCellValueFactory(new PropertyValueFactory("cantidad4"));
        totalCuentas.setCellValueFactory(new PropertyValueFactory("status"));
        try {
            // TODO
            Cuenta.mostrarCuentaCobra(conexion, listaCuenta);

        } catch (Exception ex) {
            Logger.getLogger(ReporteSemanalController.class.getName()).log(Level.SEVERE, null, ex);
        }

        float total = 0;
        for (int i = 0; i < tableCuentas.getItems().size(); i++) {
            total = total + Float.valueOf(String.valueOf(tableCuentas.getColumns().get(6).getCellObservableValue(i).getValue()));
        }

        totalPagarCobrar.setText(Float.toString(total));
    }

    private void llamarCuentaPagar() {
        listaCuenta = FXCollections.observableArrayList();
        tableCuentas.setItems(listaCuenta);
        idCuentas.setCellValueFactory(new PropertyValueFactory<>("descripcion"));
        semana1Cuentas.setCellValueFactory(new PropertyValueFactory("cantidad"));
        semana2Cuentas.setCellValueFactory(new PropertyValueFactory("cantidad1"));
        semana3Cuentas.setCellValueFactory(new PropertyValueFactory("cantidad2"));
        semana4Cuentas.setCellValueFactory(new PropertyValueFactory("cantidad3"));
        semana5Cuentas.setCellValueFactory(new PropertyValueFactory("cantidad4"));
        totalCuentas.setCellValueFactory(new PropertyValueFactory("status"));

        try {
            // TODO
            Cuenta.mostrarCuentaPagar(conexion, listaCuenta);

        } catch (Exception ex) {
            Logger.getLogger(ReporteSemanalController.class.getName()).log(Level.SEVERE, null, ex);
        }

        float total = 0;
        for (int i = 0; i < tableCuentas.getItems().size(); i++) {
            total = total + Float.valueOf(String.valueOf(tableCuentas.getColumns().get(6).getCellObservableValue(i).getValue()));
        }

        totalPagarCobrar.setText(Float.toString(total));
    }

    @FXML
    private void handleRadioBtnCuentas(ActionEvent event) {
        if (radioButtonCuentaPagar.isSelected()) {
            llamarCuentaPagar();
        } else if (radioButtonCuentaCobrar.isSelected()) {
            llamarCuentaCobrar();
        }
    }

    int optionRadioButton = 1; // ingreso -> 1 salida -> 2 gastos -> 3
    private boolean flag1 = false, flag2 = false;
    private FlujoMensual flujos = null;
    private FlujoMensual flujosSalidas = null;

    @FXML
    private void handleBtnFlujo(ActionEvent event) {

        if (comboBoxMes.getSelectionModel().getSelectedItem() != null && comboBoxAno.getSelectionModel().getSelectedItem() != null) {
            if (radioButtonFlujoEfectivoIngresos.isSelected()) {

                System.out.println("Obteniendo ingresos");
                if (!flag1) {
                    System.out.println("creando obj");
                    flujos = Conexion.getFlujos(1, comboBoxMes.getSelectionModel().getSelectedItem().substring(0, 2), comboBoxAno.getSelectionModel().getSelectedItem());
                    flag1 = true;
                    for (Flujo flujo : flujos.getSemana1()) {
                        lstFlujoSemana1.add(flujo);
                    }
                    for (Flujo flujo : flujos.getSemana2()) {
                        lstFlujoSemana2.add(flujo);
                    }
                    for (Flujo flujo : flujos.getSemana3()) {
                        lstFlujoSemana3.add(flujo);
                    }

                    for (Flujo flujo : flujos.getSemana4()) {
                        lstFlujoSemana4.add(flujo);
                    }

                }
                totalSumaSemana1.setText("$" + df2.format(flujos.getTotalSemana1()));
                tableSemana1.setItems(lstFlujoSemana1);
                tableSemana2.setItems(lstFlujoSemana2);
                totalSumaSemana2.setText("$" + df2.format(flujos.getTotalSemana2()));
                tableSemana3.setItems(lstFlujoSemana3);
                totalSumaSemana3.setText("$" + df2.format(flujos.getTotalSemana3()));
                tableSemana4.setItems(lstFlujoSemana4);
                totalSumaSemana4.setText("$" + df2.format(flujos.getTotalSemana4()));
                totalSuma.setText("$" + df2.format(flujos.getTotal()));

                System.out.println("Total semana 1:" + flujos.getTotalSemana1());
                System.out.println("Total semana 2:" + flujos.getTotalSemana2());
                System.out.println("Total semana 3:" + flujos.getTotalSemana3());
                System.out.println("Total semana 4:" + flujos.getTotalSemana4());

            } else if (radioButtonFlujoEfectivoGastos.isSelected()) {
                System.out.println("Obteniendo salidas");

                if (!flag2) {
                    System.out.println("creando obj2");
                    flujosSalidas = Conexion.getFlujos(0, comboBoxMes.getSelectionModel().getSelectedItem().substring(0, 2), comboBoxAno.getSelectionModel().getSelectedItem());
                    flag2 = true;
                    for (Flujo flujo : flujosSalidas.getSemana1()) {
                        lstFlujoSemana11.add(flujo);
                    }
                    for (Flujo flujo : flujosSalidas.getSemana2()) {
                        lstFlujoSemana22.add(flujo);
                    }
                    for (Flujo flujo : flujosSalidas.getSemana3()) {
                        lstFlujoSemana33.add(flujo);
                    }
                    for (Flujo flujo : flujosSalidas.getSemana4()) {
                        lstFlujoSemana44.add(flujo);
                    }

                }
                totalSumaSemana1.setText("$" + df2.format(flujosSalidas.getTotalSemana1()));
                tableSemana1.setItems(lstFlujoSemana11);
                tableSemana2.setItems(lstFlujoSemana22);
                totalSumaSemana2.setText("$" + df2.format(flujosSalidas.getTotalSemana2()));
                tableSemana3.setItems(lstFlujoSemana33);
                totalSumaSemana3.setText("$" + df2.format(flujosSalidas.getTotalSemana3()));
                tableSemana4.setItems(lstFlujoSemana44);
                totalSumaSemana4.setText("$" + df2.format(flujosSalidas.getTotalSemana4()));
                totalSuma.setText("$" + df2.format(flujosSalidas.getTotal()));
                System.out.println("Total semana 1:" + flujosSalidas.getTotalSemana1());
                System.out.println("Total semana 2:" + flujosSalidas.getTotalSemana2());
                System.out.println("Total semana 3:" + flujosSalidas.getTotalSemana3());
                System.out.println("Total semana 4:" + flujosSalidas.getTotalSemana4());

            } else if (radioButtonFlujoEfectivoDiferencia.isSelected()) {
                System.out.println("diferencias");

                /*checar ya que no hace las operaciones de manera correcta*/
                double total1 = 0, total2 = 0, total3 = 0, total4 = 0;

                if (!lstFlujoSemana1.isEmpty()) {
                    for (Flujo flujo : lstFlujoSemana1) {
                        total1 += flujo.getMonto();
                    }
                }
                if (!lstFlujoSemana11.isEmpty()) {
                    for (Flujo flujo : lstFlujoSemana11) {
                        total1 -= flujo.getMonto();
                    }
                }
                Flujo f1 = new Flujo();
                f1.setClasificacion("clasif");
                f1.setFecha("fecha");
                f1.setDia("dia");
                f1.setSubcategoria("subcate");
                f1.setCategoria("categoria");
                f1.setDescripcion("description");
                f1.setMonto(total1);
                        
                ///semana uno////

                if (!lstFlujoSemana2.isEmpty()) {
                    for (Flujo flujo : lstFlujoSemana2) {
                        total2 += flujo.getMonto();
                    }
                }
                if (!lstFlujoSemana22.isEmpty()) {
                    for (Flujo flujo : lstFlujoSemana22) {
                        total2 -= flujo.getMonto();
                    }
                }
                Flujo f2 = new Flujo();
                f2.setClasificacion("clasif");
                f2.setFecha("fecha");
                f2.setDia("dia");
                f2.setSubcategoria("subcate");
                f2.setCategoria("categoria");
                f2.setDescripcion("description");
                f2.setMonto(total1);
                //// semana 2 /////
                if (!lstFlujoSemana3.isEmpty()) {
                    for (Flujo flujo : lstFlujoSemana3) {
                        total3 += flujo.getMonto();
                    }
                }
                if (!lstFlujoSemana33.isEmpty()) {
                    for (Flujo flujo : lstFlujoSemana33) {
                        total3 -= flujo.getMonto();
                    }
                }
                Flujo f3 = new Flujo();
                f3.setClasificacion("clasif");
                f3.setFecha("fecha");
                f3.setDia("dia");
                f3.setSubcategoria("subcate");
                f3.setCategoria("categoria");
                f3.setDescripcion("description");
                f3.setMonto(total1);
                /// semana tres///
                if (!lstFlujoSemana4.isEmpty()) {
                    for (Flujo flujo : lstFlujoSemana4) {
                        total4 += flujo.getMonto();
                    }
                }
                if (!lstFlujoSemana44.isEmpty()) {
                    for (Flujo flujo : lstFlujoSemana44) {
                        total4 -= flujo.getMonto();
                    }
                }
                Flujo f4 = new Flujo();
                f4.setClasificacion("clasif");
                f4.setFecha("fecha");
                f4.setDia("dia");
                f4.setSubcategoria("subcate");
                f4.setCategoria("categoria");
                f4.setDescripcion("description");
                f4.setMonto(total1);

                totalSumaSemana1.setText(df2.format(total1));
                totalSumaSemana2.setText(df2.format(total2));
                totalSumaSemana3.setText(df2.format(total3));
                totalSumaSemana4.setText(df2.format(total4));
                totalSuma.setText(df2.format(total1 + total2 + total3 + total4));
                lstFlujoSemana111.add(f1);
                lstFlujoSemana222.add(f2);
                lstFlujoSemana333.add(f3);
                lstFlujoSemana444.add(f4);

                tableSemana1.setItems(lstFlujoSemana111);
                tableSemana2.setItems(lstFlujoSemana222);
                tableSemana3.setItems(lstFlujoSemana333);
                tableSemana4.setItems(lstFlujoSemana444);

            }

        } else {
            alert.setHeaderText("Debe seleccionar una fecha");
            alert.setContentText("No se puede consultar sin un mes y un año");
            alert.showAndWait();
        }
    }

    @FXML
    private void handleBtnBack() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/vistas/home.fxml"));
        Stage window = (Stage) btnBack.getScene().getWindow();
        window.setScene(new Scene(root));
    }

}
