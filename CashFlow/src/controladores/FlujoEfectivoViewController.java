/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controladores;

import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import modelo.Categoria;
import modelo.Conexion;
import modelo.Flujo;

/**
 * FXML Controller class
 *
 * @author Luis Manuel
 */
public class FlujoEfectivoViewController implements Initializable {

    @FXML
    private TextField txtMonto;

    @FXML
    private Button btnBack;

    @FXML
    private TableColumn subCategoryColumn;

    @FXML
    private TableColumn montoColumn;

    @FXML
    private TextField txtDescription;

    @FXML
    private CheckBox checkIngresos;

    @FXML
    private CheckBox checkSalidas;

    @FXML
    private TableColumn dateColumn;

    @FXML
    private ComboBox<Categoria> comboBoxCategoria;

    @FXML
    private TableView<Flujo> flujoTable;

    @FXML
    private TableColumn categoryColumn;

    @FXML
    private Button btnRegistro;

    @FXML
    private TableColumn descriptionColumn;

    ObservableList<Categoria> lstCategory;
    ObservableList<Flujo> lstFlujo;

    Alert alert = new Alert(Alert.AlertType.INFORMATION);

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        Conexion.getConnection();
        lstFlujo = FXCollections.observableArrayList();
        lstCategory = FXCollections.observableArrayList();
        dateColumn.setCellValueFactory(new PropertyValueFactory("fecha"));
        descriptionColumn.setCellValueFactory(new PropertyValueFactory("descripcion"));
        categoryColumn.setCellValueFactory(new PropertyValueFactory("categoria"));
        subCategoryColumn.setCellValueFactory(new PropertyValueFactory("subcategoria"));
        montoColumn.setCellValueFactory(new PropertyValueFactory("monto"));
        checkIngresos.setSelected(true);

        fullComboBox();
        fullTable();
    }

    @FXML
    public void handleBtnBack() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/vistas/home.fxml"));
        Stage window = (Stage) btnBack.getScene().getWindow();
        window.setScene(new Scene(root));
    }

    @FXML
    public void handleBtnRegister() {
        double monto = 0;

        if (checkIngresos.isSelected() && checkSalidas.isSelected()) {
            alert.setHeaderText("Entrada invalida");
            alert.setContentText("Debe seleccionar solo una opcion ");
            alert.showAndWait();
        }
        if (!checkIngresos.isSelected() && !checkSalidas.isSelected()) {
            alert.setHeaderText("Entrada invalida");
            alert.setContentText("Debe seleccionar alguna opcion ");
            alert.showAndWait();
        }
        if (checkIngresos.isSelected() && !checkSalidas.isSelected() || !checkIngresos.isSelected() && checkSalidas.isSelected()) {
            if (!"".equals(txtDescription.getText()) && !"".equals(txtMonto.getText())
                    && comboBoxCategoria.getSelectionModel().getSelectedItem() != null) {

                Date date = new Date();
                SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                System.out.println(formatter.format(date));
                try {
                    Flujo f = new Flujo(
                            monto = Double.parseDouble(txtMonto.getText()),
                            formatter.format(date),
                            txtDescription.getText(),
                            comboBoxCategoria.getSelectionModel().getSelectedItem().getCategoria(),
                            comboBoxCategoria.getSelectionModel().getSelectedItem().getSubcategoria(),
                            "salida"
                    );
                    if (Conexion.setFlujo(f)) {
                        alert.setHeaderText("Informacion");
                        alert.setContentText("Guardado con exito");
                        alert.showAndWait();
                        lstFlujo.add(f);
                        this.flujoTable.setItems(lstFlujo);
                    } else {
                        alert.setContentText("Ha ocurrido un error n:174, contacte a sistemas");
                        alert.showAndWait();

                    }
                } catch (NumberFormatException e) {
                    alert.setHeaderText("Entrada invalida en monto");
                    alert.setContentText("Debe ingresar solo numeros por favor");
                    alert.showAndWait();
                }

            } else {

                alert.setHeaderText("¡Aviso!");
                alert.setContentText("Debe de llenar los campos o registrar categorias");
                alert.showAndWait();
            }
        }

    }

    private void fullComboBox() {

        if (Conexion.getCategorias() != null) {
            for (Categoria c : Conexion.getCategorias()) {
                lstCategory.add(c);
            }
            comboBoxCategoria.setItems(lstCategory);
        }
    }

    public void fullTable() {
        if (Conexion.getFlujos() != null) {
            for (Flujo flujo : Conexion.getFlujos()) {
                lstFlujo.add(flujo);
            }
            this.flujoTable.setItems(lstFlujo);
        }

    }

}
